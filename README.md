# ajicpaths

A Stata command to create $dopath and $datapath globals using a dialog box.

## Installation
~~~
    net from https://gitlab.com/tpayne9/ajicpath/-/raw/master/

    net install ajicpath
    
    help ajicpath
~~~

